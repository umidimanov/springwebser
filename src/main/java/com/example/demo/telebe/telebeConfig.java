package com.example.demo.telebe;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

import static java.time.Month.*;

@Configuration
public class telebeConfig {

    @Bean
    CommandLineRunner commandLineRunner(telebeRepository repository) {
        return args -> {
           telebe umid = new telebe(
                    "Ümid",
                    LocalDate.of(1999, DECEMBER, 9),
                    "umid.imanoff@gmail.com"
            );
            telebe nermin = new telebe(
                    "Nərmin",
                    LocalDate.of(2001, FEBRUARY, 3),
                    "narmin.imanova@gmail.com"
            );
            List<telebe> foo = new ArrayList<telebe>();
            foo.add(umid);
            foo.add(nermin);
            System.out.println("size="+foo.size());
            repository.saveAll(
                    foo
            );
        };
    }
}
