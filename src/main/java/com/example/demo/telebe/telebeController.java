package com.example.demo.telebe;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(path = "api/v1/telebe")
public class telebeController {

    private final telebeService tservice;

    @Autowired
    public telebeController(telebeService tservice) {
        this.tservice = tservice;
    }

    @GetMapping
    public List<telebe> gettelebeler() {
        return tservice.gettelebeler();
    }

    @PostMapping
    public void registerNewTelebe(@RequestBody telebe student){
        tservice.addNewTelebe(student);
    }

    @DeleteMapping(path = "{telebeId}")
    public void deleteTelebe(@PathVariable("telebeId") Long telebeId){
         tservice.deleteTelebe(telebeId);
    }

    @PutMapping(path = "{telebeId}")
    public void updateTelebe(@PathVariable("telebeId") Long telebeId,@RequestParam(required = false) String name,@RequestParam(required = false) String email){
        tservice.updateTelebe(telebeId,name,email);
    }
}
