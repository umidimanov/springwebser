package com.example.demo.telebe;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface telebeRepository extends JpaRepository<telebe, Long> {

    // Select * from telebe where email = ?
    @Query("select t from telebe t where t.email = ?1")
    Optional<telebe> findtelebeByEmail(String email);
}
