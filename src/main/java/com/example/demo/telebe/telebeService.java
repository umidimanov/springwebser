package com.example.demo.telebe;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Component
public class  telebeService {

    private final telebeRepository studentRepository;

    @Autowired
    public telebeService(com.example.demo.telebe.telebeRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    public void addNewTelebe(telebe student) {
        Optional<telebe> telebeOptional = studentRepository.findtelebeByEmail(student.getEmail());
        if (telebeOptional.isPresent()){
            throw new IllegalStateException("email taken");
        }
        studentRepository.save(student);
    }

    public List<telebe> gettelebeler() {
          return studentRepository.findAll();
    }

    public void deleteTelebe(Long telebeId) {
     boolean exists = studentRepository.existsById(telebeId);
     if (!exists){
         throw new IllegalStateException("telebe id" +telebeId+"does not exists");
     }
        studentRepository.deleteById(telebeId);
    }

    @Transactional
    public void updateTelebe(Long telebeId, String name, String email) {

        telebe student = studentRepository.findById(telebeId).orElseThrow(() ->
                new IllegalStateException("Telebe with id "+ telebeId +" does not exist"));

        if(name != null && name.length() > 0 && !Objects.equals(student.getName(), name)){
            student.setName(name);
        }
        if(email != null && email.length() > 0 && !Objects.equals(student.getEmail(), email)){
            Optional<telebe> telebeOptional = studentRepository.findtelebeByEmail(email);
            if (telebeOptional.isPresent()){
                throw new IllegalStateException("email taken");
            }
            student.setEmail(email);
        }
    }
}
